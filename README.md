CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

RSVP flag custom action link to allow users to RSVP for events.

This provides a display for the flag that will show a list of
user pictures for each person who has confirmed they will attend
the event.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/flag_rsvp

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/flag_rsvp

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://www.drupal.org/project/views)
 * Flag (https://www.drupal.org/project/flag)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To use this formatter - you must:
* Create a flag with "RSVP Link" as the link type

MAINTAINERS
-----------
Current maintainers:
 * Damyon Wiese - https://www.drupal.org/user/3616877

This project has been sponsored by:
 * Doghouse Agency - specializing in developing drupal solutions . Visit https://doghouse.agency/ for more information
