<?php

namespace Drupal\flag_rsvp\Plugin\ActionLink;

use Drupal\Core\Entity\EntityInterface;
use Drupal\flag\FlagInterface;
use Drupal\flag\Plugin\ActionLink\AJAXactionLink;

/**
 * Provides the RSVP link type.
 *
 * This class is an extension of the Ajax link type, but modified to
 * provide list of accepted users.
 *
 * @ActionLinkType(
 *   id = "rsvp_link",
 *   label = @Translation("RSVP Link"),
 *   description = "An AJAX action link which displays the list of attendees."
 * )
 */
class RSVPLink extends AJAXactionLink {

  /**
   * {@inheritdoc}
   */
  public function getAsFlagLink(FlagInterface $flag, EntityInterface $entity) {

    // Get the render array.
    $build = parent::getAsFlagLink($flag, $entity);

    $build['#theme'] = 'flag_rsvp';

    return $build;
  }

}
