<?php

namespace Drupal\flag_rsvp\TwigExtension;

use Drupal\Core\Template\TwigExtension;
use Drupal\views\Views;

/**
 * Provides a Twig extension to get a list of flaggers.
 */
class FlagMembers extends TwigExtension {

  /**
   * Generates a list of all Twig functions that this extension defines.
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction(
        'flagmembers',
        [$this, 'members'],
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'flag.twig.members';
  }

  /**
   * Gets the list of flaggers for the given flag and flaggable.
   *
   * @param mixed $flag
   *   The flag entity.
   * @param mixed $flaggable
   *   The flaggable entity.
   *
   * @return string
   *   The list of flaggers.
   */
  public static function members($flag = NULL, $flaggable = NULL) {
    if (!$flag || !$flaggable) {
      return '';
    }

    $view = Views::getView('flag_rsvp');
    if (!$view) {
      $logger = \Drupal::service('logger.factory')->get('flag_rsvp');
      $logger->warning('flag_rsvp view does not exist or is disabled.');
      return '';
    }
    $view->setArguments([$flaggable->id()]);
    $view->setDisplay('embed');
    $view->get_total_rows = TRUE;
    $view->execute();

    // Get the results of the view.
    $view_result = $view->render();

    $summary = '';
    $total = $view->total_rows;
    if ($total) {
      $summary = \Drupal::service('string_translation')->formatPlural($total, '@count person is going.', '@count people are going.');
    }

    $itemlist = [
      '#type' => 'container',
      'members' => $view_result,
      '#suffix' => $summary,
    ];

    return render($itemlist);
  }

}
